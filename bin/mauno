#!/usr/bin/env node

const { ArgumentParser } = require('argparse');

const { fetchLunchMenu } = require('../index');
const { version } = require('../package.json');

const parser = new ArgumentParser({ description: 'Scrapes lunch information from Mauno\'s Web site.' });

parser.add_argument('-v', '--version', { action: 'version', version, help: 'Display version number.' });
parser.add_argument('-d', '--day', { help: 'Day of week of which lunch menu to retrieve.' });
parser.add_argument('-m', '--mauno', { help: 'Which Mauno restaurant to retrieve lunch menu of.' });
parser.add_argument('-t', '--tomorrow', { action: 'store_true', help: 'Display tomorrow\'s lunch.' });
parser.add_argument('-w', '--week', { action: 'store_true', help: 'Display lunch menu for entire week.' });

const args = parser.parse_args();

const getDayOfWeek = () => {
  const mapping = {
    mon: 1,
    tue: 2,
    wed: 3,
    thu: 4,
    fri: 5,
  };

  if (args.day != null) {
    const day = mapping[args.day.toLowerCase()];

    if (day != null) {
      return day;
    }

    throw new Error(`Unrecognized day of week: ${args.day}`);
  } else {
    const dayOfWeek = new Date().getDay();

    return args.tomorrow ? dayOfWeek < 7 ? dayOfWeek + 1 : 1 : dayOfWeek;
  }
};

fetchLunchMenu(args.mauno)
  .then((allDays) => {
    const weekDayMapping = {
      0: 'Monday',
      1: 'Tuesday',
      2: 'Wednesday',
      3: 'Thursday',
      4: 'Friday',
      5: 'Saturday',
      6: 'Sunday',
    };
    const printDay = (index, day) => {
      process.stdout.write(`Lunch for ${weekDayMapping[index]}:\n`);
      process.stdout.write(`${day.trim()}\n`);
    };

    if (args.week) {
      allDays.forEach((day, index) => {
        printDay(index, day);
        if (index + 1 < allDays.length) {
          process.stdout.write('\n');
        }
      });
    } else {
      const dayOfWeek = getDayOfWeek();
      const index = dayOfWeek === 0 ? 6 : dayOfWeek - 1;
      const day = allDays[index];

      if (day) {
        printDay(index, day);
      } else {
        process.stderr.write(`Couldn't find lunch for ${weekDayMapping[index]}.\n`);
        process.exit(1);
      }
    }
  })
  .catch((err) => {
    console.error(err);
    process.stderr.write('Couldn\'t connect to Mauno\'s Web site.\n');
    process.exit(1);
  });
