const fetch = require('cross-fetch');
const cheerio = require('cheerio');
const isBlank = require('is-blank');

const maunos = {
  biocity: 'https://www.mauno.fi/fi/ravintolat/mauno-biocity',
  electrocity: 'https://www.mauno.fi/fi/ravintolat/mauno-electrocity',
  triviumcity: 'https://www.mauno.fi/fi/ravintolat/mauno-triviumcity',
};

module.exports.fetchLunchMenu = (mauno = 'biocity') => {
  const url = maunos[mauno];

  if (!url) {
    return Promise.reject(new Error(`Unrecognized Mauno: ${mauno}`));
  }

  return fetch(url)
    .then(response => response.text())
    .then(body => {
      const $ = cheerio.load(body);
      const week = [];

      $('#lunch .week .column').each((index, day) => {
        const text = $(day)
          .text()
          .split(/(\r?\n)+/)
          .filter(line => !isBlank(line))
          .map(line => line.trim())
          .join('\n')
          .trim();

        week[index] = text;
      });

      return week;
    });
};
