# Mauno lunch menu scraper

Scrapes today's lunch menu from Mauno's [web site]. Supports all three Mauno
restaurants.

[web site]: https://www.mauno.fi

## Installation

```bash
$ npm install -g @treet/mauno-scraper
```

## Usage

```bash
$ mauno -m [biocity|electrocity|triviumcity]
```

You can also specify the day of the week (Mon-Fri) as command line argument. If
omitted, current day of the week is used instead:

```bash
$ mauno --day wed
```
